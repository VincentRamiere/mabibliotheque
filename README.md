# Ajout de livres dans une bibliotheque

## Auteur
 - Nom : RAMIERE
 - Prénom : Vincent
 - Métier : Concepteur-developpeur

## Description
Lorsque l'utilisateur a terminé de saisir sa liste de livres, le programme retourne la liste.

## Installation
Lancer le script "install.sh"

## Désinstallation
Lancer le script **uninstall.sh**

