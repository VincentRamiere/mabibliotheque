import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MesLivres {
    public static void main(String[] args) {
        List<String> livres = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Veuillez entrer les noms des livres (tapez 'fin' pour terminer la liste) :");
        
        String livre = scanner.nextLine();
        while (!livre.equalsIgnoreCase("fin")) {
            livres.add(livre);
            livre = scanner.nextLine();
        }
        
        System.out.println("Liste des livres saisis :");
        for (String nomLivre : livres) {
            System.out.println(nomLivre);
        }
    }
}

